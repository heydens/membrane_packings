from turtle import position
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import least_squares
from scipy.sparse import coo_matrix
import seaborn as sns
from scipy.ndimage import gaussian_filter
import csv

n = 60 # Take n even
s = 1/(n+1)
b = 1
p = 10000

x = np.arange(n + 2)
y = np.arange(n + 2)
X, Y = np.meshgrid(x, y)

position_unknowns = []
for int_row in range(1,n+1):
    for int_column in range(1,n+1):
        if int_row >= int(n/4) and int_row <= n-int(n/4)+1:
            position_unknowns.append([int_row,int_column])

position_zeros = []
for int_row in range(0,n+2):
    for int_column in range(0,n+2):
        if int_row < int(n/4) or int_row > n-int(n/4)+1:
            position_zeros.append([int_row,int_column])

def energy(u, n, b, p, **kwargs):
    v = np.zeros((n + 2, n + 2))
    u = u.reshape((n, n))
    v[1:-1, 1:-1] = u

    for int_zeros in range(0,len(position_zeros)):
        v[position_zeros[int_zeros][0],position_zeros[int_zeros][1]] = 0

    y = b * ( (v[2:, 1:-1]-2*u+v[:-2, 1:-1])**2 + (v[1:-1, 2:]-2*u+v[1:-1, :-2])**2 + 1/8*(v[2:, 2:]-v[:-2, 2:]-v[2:, :-2]+v[:-2, :-2])**2 ) + \
        p * ( (v[2:, 1:-1]-v[:-2, 1:-1])**2 + (v[1:-1, 2:]-v[1:-1, :-2])**2 - 4*s**2 ) **2
    return y.ravel()

def compute_jac_indices(n):
    i = np.arange(n)
    jj, ii = np.meshgrid(i, i)

    ii = ii.ravel()
    jj = jj.ravel()

    ij = np.arange(n**2)

    jac_rows = [ij]
    jac_cols = [ij]

    mask = ii > 0
    ij_mask = ij[mask]
    jac_rows.append(ij_mask)
    jac_cols.append(ij_mask - n)

    mask = ii < n - 1
    ij_mask = ij[mask]
    jac_rows.append(ij_mask)
    jac_cols.append(ij_mask + n)

    mask = jj > 0
    ij_mask = ij[mask]
    jac_rows.append(ij_mask)
    jac_cols.append(ij_mask - 1)

    mask = jj < n - 1
    ij_mask = ij[mask]
    jac_rows.append(ij_mask)
    jac_cols.append(ij_mask + 1)

    return np.hstack(jac_rows), np.hstack(jac_cols)

jac_rows, jac_cols = compute_jac_indices(n)

u0 = np.ones(n**2) * 0.5
jac_rows, jac_cols = compute_jac_indices(n)

res = least_squares(energy, u0, gtol=1e-3, args=(n,b,p), kwargs={'jac_rows': jac_rows, 'jac_cols': jac_cols}, verbose=1)

res_v = np.zeros((n + 2, n + 2))
res_u = res.x.reshape((n, n))
res_v[1:-1, 1:-1] = res_u

grad_x = np.zeros((n+2, n+2))
grad_y = np.zeros((n+2, n+2))
gaussian_curv = np.zeros((n+2, n+2))
length = np.zeros((n+2, n+2))

dataset = []
for pos in position_unknowns:
        
        val_x = res_v[pos[0]+1, pos[1]] - res_v[pos[0]-1, pos[1]]
        val_y = res_v[pos[0], pos[1]+1] - res_v[pos[0], pos[1]-1]

        grad_x[pos[0], pos[1]] = val_x/np.linalg.norm([val_x,val_y])
        grad_y[pos[0], pos[1]] = val_y/np.linalg.norm([val_x,val_y])

        length[pos[0],pos[1]] = np.linalg.norm([val_x,val_y])**2/(4*s**2)

for pos in position_unknowns:
        grad_x_grad_x = grad_x[pos[0]+1, pos[1]] - grad_x[pos[0]-1, pos[1]]
        grad_x_grad_y = grad_x[pos[0], pos[1]+1] - grad_x[pos[0], pos[1]-1]
        grad_y_grad_y = grad_y[pos[0], pos[1]+1] - grad_y[pos[0], pos[1]-1]
        grad_y_grad_x = grad_y[pos[0]+1, pos[1]] - grad_y[pos[0]-1, pos[1]]

        eigenvalues = np.linalg.eigvals([[grad_x_grad_x,grad_x_grad_y],[grad_y_grad_x,grad_y_grad_y]])

        gaussian_curv[pos[0], pos[1]] = eigenvalues[0] * eigenvalues[1]

        dataset.append([pos[0],pos[1],grad_x[pos[0], pos[1]],grad_y[pos[0], pos[1]],length[pos[0],pos[1]],gaussian_curv[pos[0], pos[1]]])

csv_file = 'dataset.csv'
with open(csv_file, 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(dataset)

smoothed_length = gaussian_filter(length, sigma=2.0)

plt.figure(figsize=(10, 10))
cmap = sns.color_palette("Blues", as_cmap=True)
plt.imshow(smoothed_length, cmap=cmap, alpha=0.6, vmin=min(length.ravel()), vmax=max(length.ravel()))
plt.colorbar(use_gridspec=True, fraction=0.01, pad=0.04)
plt.quiver(X, Y, grad_x, grad_y, scale=40, color='bisque', alpha=1, linewidth=10, headaxislength=0, headlength=0)
plt.axis('off')
plt.show()